const path = require('path')
const { DllPlugin } = require('webpack')
module.exports = {
  mode: 'development',
  entry: {
    lodash: ['lodash']
  },
  output: {
    path: path.resolve(__dirname, './dll'),
    filename: '[name].dll.js',
    library: 'lodash'
  },
  plugins: [
    new DllPlugin({
      path: path.join(__dirname, './dll', '[name]-manifest.json'),
      name: 'lodash'
    })
  ]
}
